﻿# Chat Hack Protocol

## Summary

Chat Hack Protocol is a protocol that may be used to implement a simple messaging app, supporting authenticated and non-authenticated users, public messaging, and private exchange of messages and files between two users. It uses a client/server architecture and is designed to function on top of TCP ; integrity of data and reliability of exchanges will be more important than the transmission speed.

## Acknowledgements

This protocol has been designed by Alexandre Miranda and Jonathan Poinhos, both students at Université Gustave Eiffel in Champs-sur-Marne, France. This is part of a project in network programming.

## Overview of the protocol

The client must first connect to the server, either using an username and password combination registered in the server's database (CNP), or using a temporary username not registered in the server's database (CNU). The server then notifies the client that the connection is approved (CRA) or rejected (CRD). When the connection is approved, the client is able to send public messages and private connection requests. If the connection is refused, the TCP connection is closed, and it's up to the client to connect again if they want to retry. The client may not send any frame before the server has approved the connection, or the server will close the connection.

When the user sends a public message (PBS), the server sends the message back to all connected users (PBB).

When user A sends a private connection request (PVR) to another user B, the server transmits the request to user B (PVRT) if it is connected, or sends a request error to user A (PVRE) if not. User B can then either approve or reject the request.

When user B approves the request, user B sends to the server a port number on which he is ready to listen (PVA). The server generates a random 128-bit key. The server sends a packet containing the key to user B (PVK), then sends a packet containing the key, user B's IP and port to user A (PVAT). User A is now able to connect privately to user B. Both users can then send private messages (PVM) and private files (PVF) to each other. Each private message or file must include the key, otherwise the private connection is closed.

When user B rejects the request (PVD), the server notifies user A that the private connection request was rejected (PVDT)

A user may close the connection with the server at anytime, the server will then stop sending them public messages, private communication requests and responses. A private communication ends when one of the users close the connection.

## Data types

Data transmitted via this protocol may be of the following types:
* `byte`: a signed 8-bit numerical value, in big endian
* `int`: a signed 32-bit numerical value, in big endian
* `long`: a signed 64-bit numerical value, in big endian
* `string`: a string of characters encoded in UTF-8. The number of bytes needed to encode the string must be transmitted beforehand, unless known in advance
* `bytes`: an uninterpreted sequence of bytes that can represent any kind of data. The number of bytes in the sequence must be transmitted beforehand, unless known in advance

## Opcodes

| Opcode | Operation | Mnemonic |
|---|---|---|
| 1 | Connection Request, Username Only | CNU |
| 2 | Connection Request, Username and Password | CNP |
| 3 | Public Message Sending | PBS |
| 4 | Private Connection Request | PVR |
| 5 | Private Connection Approval | PVA |
| 6 | Private Connection Denial | PVD |
| 7 | Private Message Sending | PVM |
| 8 | Private File Sending | PVF |
| 9 | Connection Request Approval | CRA |
| 10 | Connection Request Denial | CRD |
| 11 | Public Message Broadcasting | PBB |
| 12 | Private Connection Request Error | PVRE |
| 13 | Private Connection Request Transmission | PVRT |
| 14 | Private Connection Approval Transmission | PVAT |
| 15 | Private Connection Denial Transmission | PVDT |
| 16 | Private Connection Key | PVK |

## Client Packets

### Connection Request, Username Only (CNU)

| byte | int | string |
|---|---|---|
| opcode | username length | username |

If the username is available, the server will respond with a CRA packet.
If the username is already in use or present in the server's database, the server will respond with a CRD packet.

### Connection Request, Username and Password (CNP)

| byte | int | string | int | string |
|---|---|---|---|---|
| opcode | username length | username | password length | password |

If the username and password correspond to an existing entry in the server's database, the server will respond with a CRA packet.
If this is not the case, the server will respond with a CRD packet. 

### Public Message Sending (PBS)

| byte | int | string |
|---|---|---|
| opcode | message length | message |

The server will send a PBB packet to all connected clients, broadcasting the message in the public chat.

### Private Communication Request (PVR)

| byte | int | string |
|---|---|---|
| opcode | recipient length | recipient |

If there is a client connected under the specified username, the server will send it a PVRT packet.
If not, the server will respond with a PVRE packet.

### Private Connection Approval (PVA)

| byte | int | string | int |
|---|---|---|---|
| opcode | sender length | sender | port |

The server will respond with a PVK packet containing a key, and will send a PVAT packet with the same key + IP + port to the other user.

### Private Connection Denial (PVD)

| byte | int | string |
|---|---|---|
| opcode | sender length | sender |

The server will send a PVDT packet to the sender of the private communication request.

### Private Message Sending (PVM)

| byte | 16 bytes | int | string |
|---|---|---|---|
| opcode | key | message length | message |

Sends a private message to the specified user.

### Private File Sending (PVF)

| byte | 16 bytes | int | string | int | bytes |
|---|---|---|---|---|---|---|
| opcode | key | filename length | filename | file size | file |

Sends a private file to the specified user.

## Server Packets

### Connection Request Approval (CRA)

| byte |
|---|
| opcode |

The client will be able to send public messages and initiate or receive private communication requests.

### Connection Request Denial (CRD)

| byte | byte |
|---|---|
| opcode | errcode |

The client will be unable to send public messages and initiate or receive private communication requests.
`errcode` can be:

* 1 = attempted to connect without password, username was taken
* 2 = attempted to connect with a password, username was unregistered
* 3 = attempted to connect with a password, password was invalid

### Public Message Broadcasting (PBB)

| byte | int | string | int | string |
|---|---|---|---|---|
| opcode | author length | author | message length | message |

### Private Connection Request Error (PVRE)

| byte | int | string |
|---|---|---|
| opcode | recipient length | username |

### Private Connection Request Transmission (PVRT)

| byte | int | string |
|---|---|---|
| opcode | sender length | sender |

Transmits a private connection request to the receiving client.

### Private Connection Approval Transmission (PVAT)

| byte | int | string | 16 bytes | 4 bytes | int |
|---|---|---|---|---|---|
| opcode | recipient length | recipient | key | ipv4 address | port |

### Private Connection Denial Transmission (PVDT)

| byte | int | string |
|---|---|---|
| opcode | recipient length | recipient |

### Private Connection Key (PVK)

| byte | int | string | 16 bytes |
|---|---|---|
| opcode | username length | username | key |

## Premature close and handling of invalid packets

If an invalid packet is received (in other words, that doesn't respect the protocol), the recipient is expected to drop them and to close the connection with the sender. If the end of stream is reached before finishing reading a packet, the packet should be ignored as well.

## Security Considerations

Chat Hack Protocol transmits everything in plain text, including passwords, message contents, files and IP addresses of clients etablishing a private communication. It should not be used in contexts where confidentiality of data is crucial.

## Authors

Alexandre Miranda (amiranda@etud.u-pem.fr)
Jonathan Poinhos (jpoinhos@etud.u-pem.fr)
