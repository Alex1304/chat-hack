# ChatHack

A simple IRC-like chat with server and client, created with Java 14.

## Features

* Authentication supported using a password server
* Public messaging
* Private connection to another user
* Private messaging and file sending

## Installation

* Have [Java](https://www.java.com) 11 or more installed
* Download [ServerMDP](http://igm.univ-mlv.fr/coursprogreseau/tds/projet2020/ServerMDP.jar) to support authentication
* Create a username database
  * Format is an UTF-8 .txt file where each line is `username$password`
  * [Download example database](http://igm.univ-mlv.fr/coursprogreseau/tds/projet2020/passwords.txt)

* Use [Apache Ant](https://ant.apache.org/) to build using `build.xml`

```shell
ant
```

## Launch Password Server

* Run `ServerMDP.jar`

```shell
java -jar ServerMDP.jar {auth_server_port} {database_file}
```

## Launch Server

* Run `chathack-server.jar`

```shell
java -jar chathack-server.jar {server_port} {auth_server_address} {auth_server_port}
```

## Launch Client

* Run `chathack-client.jar`
  * With password : authenticated connection (user must be registered in the database)
  * No password : unauthenticated connection (username must not be registered in the database)

```shell
java -jar chathack-client.jar {host} {port} {file_directory} {username} [password]
```

## Client Commands

* Send public message

```shell
{message}
```

* Send private message

> If there is no private connection established with the user yet, sending a message will automatically send a request

```shell
@{username} {message}
```

* Send private file

> Sends a file from the sender's `file_directory` to the receiver's `file_directory`
>
> Supports relative paths

```shell
/{login} {file}
```

* Accept private connection request

```shell
!accept {username}
```

* Reject private connection request

```shell
!reject {username}
```

* Close private connection

```shell
!close {username}
```

