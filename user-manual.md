# ChatHack User Manual

## Implemented Features

* Connect with or without authentication
* Authentication using an authenticating server with database
* Public messaging
* Private messaging
* Private file sending

## Unimplemented Features

* Encrypted database
* Encrypted connection

>  Do not use this program for sharing confidential information

## Launch Client

* Run `chathack-client.jar`
  * With password : authenticated connection (user must be registered in the database)
  * No password : unauthenticated connection (username must not be registered in the database)

```shell
java -jar chathack-client.jar {host} {port} {file_directory} {username} [password]
```

## User Commands

* Send public message

```shell
{message}
```

* Send private message

> If there is no private connection established with the user yet, sending a message will automatically send a request

```shell
@{username} {message}
```

* Send private file

> Sends a file from the sender's `file_directory` to the receiver's `file_directory`
>
> Supports relative paths

```shell
/{login} {file}
```

* Accept private connection request

```shell
!accept {username}
```

* Reject private connection request

```shell
!reject {username}
```

* Close private connection

```shell
!close {username}
```

## Known Issues

* If authentication fails because a wrong password was entered : when retrying to connect with the same username, the server will reject the connection request even if the password is correct, telling the username is already taken.
* When receiving a file with the same name as another file already present in `file_directory`, the original file is overwritten.