package fr.upem.net.chathack.client;

import java.lang.ref.WeakReference;
import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Observes user input in a separate thread, filling input in a blocking queue.
 */
public class UserInputObserver implements Runnable {
	
	private final ArrayList<WeakReference<UserInputListener>> listeners = new ArrayList<>();
	
	private final Selector selector;
	private final Runnable onTerminate;
	private final BlockingQueue<String> inputQueue = new ArrayBlockingQueue<>(1);
	
	public UserInputObserver(Selector selector, Runnable onTerminate) {
		this.selector = selector;
		this.onTerminate = onTerminate;
	}
	
	@Override
	public void run() {
		var scanner = new Scanner(System.in);
		while (scanner.hasNextLine()) {
			try {
				var input = scanner.nextLine();
				inputQueue.put(input);
				selector.wakeup();
			} catch (InterruptedException e) {
				break;
			}
		}
		scanner.close();
		onTerminate.run();
	}
	
	public void processQueuedInput() {
		var input = inputQueue.poll();
		if (input != null) {
			var it = listeners.iterator();
			while (it.hasNext()) {
				var ref = it.next();
				var listener = ref.get();
				if (listener == null) {
					it.remove();
					continue;
				}
				listener.onUserInput(input);
			}
		}
	}

	public void addListener(UserInputListener listener) {
		listeners.add(new WeakReference<>(listener));
	}
}
