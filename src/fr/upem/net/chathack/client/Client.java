package fr.upem.net.chathack.client;

import static java.nio.channels.SelectionKey.OP_ACCEPT;
import static java.nio.channels.SelectionKey.OP_CONNECT;
import static java.nio.channels.SelectionKey.OP_READ;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.upem.net.chathack.protocol.NetworkContext;
import fr.upem.net.chathack.protocol.reader.ChatHackFrameReader;

/**
 * Main classs for client.
 */
public class Client {
	private static final Logger LOGGER = Logger.getLogger(Client.class.getName());

	private final ServerSocketChannel privateServerSocket;
	private final SocketChannel chathackSocket;
	private final Selector selector;
	private final ClientState state;
	private final UserInputObserver inputObserver;
	private Thread clientThread;
	
	private Client(ServerSocketChannel privateServerSocket, SocketChannel chathackSocket, Selector selector, ClientState state) {
		this.privateServerSocket = privateServerSocket;
		this.chathackSocket = chathackSocket;
		this.selector = selector;
		this.state = state;
		this.inputObserver = new UserInputObserver(selector, this::shutdown);
	}

	public static Client create(String host, int port, String dir, String username, String password) throws IOException {
		// Configure socket to chathack server
		var chathackSocket = SocketChannel.open();
		chathackSocket.configureBlocking(false);
		chathackSocket.connect(new InetSocketAddress(host, port));
		// Configure server socket for accepting private connections
		var privateServerSocket = ServerSocketChannel.open();
		privateServerSocket.configureBlocking(false);
		privateServerSocket.bind(null);
		var privateServerPort = ((InetSocketAddress) privateServerSocket.getLocalAddress()).getPort();
		LOGGER.info("Accepting private connections on port " + privateServerPort);
		// Configure selector
		var selector = Selector.open();
		chathackSocket.register(selector, OP_CONNECT);
		privateServerSocket.register(selector, OP_ACCEPT);
		// Configure state
		var fileDirectory = Paths.get(dir);
		var state = new ClientState(selector, fileDirectory, privateServerPort, username, password);
		return new Client(privateServerSocket, chathackSocket, selector, state);
	}
	
	public void launch() throws IOException {
		// Run user input observer on separate thread
		var inputObserverThread = new Thread(inputObserver);
		inputObserverThread.setDaemon(true);
		inputObserverThread.start();
		clientThread = Thread.currentThread();
		// main loop
		while (!Thread.interrupted()) {
			try {
				selector.select(this::treatKey);
				inputObserver.processQueuedInput();
			} catch (UncheckedIOException tunneled) {
				throw tunneled.getCause();
			}
		}
	}
	
	public void shutdown() {
		clientThread.interrupt();
		selector.wakeup();
	}
	
	private void treatKey(SelectionKey key) {
		try {
			if (key.isValid() && key.isAcceptable()) {
				doAccept(key);
			}
			if (key.isValid() && key.isConnectable() && key.channel() == chathackSocket) {
				doConnectToServer(key);
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		try {
			if (key.isValid() && key.isConnectable() && key.channel() != chathackSocket) {
				doConnectToPrivateClient(key);
			}
			if (key.isValid() && key.isWritable()) {
				((NetworkContext) key.attachment()).doWrite();
			}
			if (key.isValid() && key.isReadable()) {
				((NetworkContext) key.attachment()).doRead();
			}
		} catch (IOException | CancelledKeyException e) {
			LOGGER.log(Level.WARNING, "Closing due to IOException", e);
			silentlyClose(key);
		}
	}
	
	private void doAccept(SelectionKey key) throws IOException {
		SocketChannel acceptedClientSocket = privateServerSocket.accept();
		if (acceptedClientSocket == null) {
			return;
		}
		acceptedClientSocket.configureBlocking(false);
		var acceptedClientKey = acceptedClientSocket.register(selector, OP_READ);
		configurePrivateClient(null, null, acceptedClientKey);
	}

	private void doConnectToServer(SelectionKey key) throws IOException {
		if (!chathackSocket.finishConnect()) {
			return;
		}
		key.interestOps(OP_READ);
		var handle = new ChathackServerHandle(state);
		var networkContext = new NetworkContext(key, new ChatHackFrameReader(), handle, false);
		networkContext.setCloseHandler(() -> {
			LOGGER.severe("Terminating program due to connection closed by server");
			shutdown();
		});
		handle.setNetworkContext(networkContext);
		state.setServerHandle(handle);
		key.attach(networkContext);
		inputObserver.addListener(handle);
		handle.initConnection();
		LOGGER.info("Successfully connected to server");
	}
	
	private void doConnectToPrivateClient(SelectionKey key) throws IOException {
		var privateClientSocket = (SocketChannel) key.channel();
		if (!privateClientSocket.finishConnect()) {
			return;
		}
		var pending = state.pendingConnections().get(privateClientSocket);
		if (pending == null) {
			// We didn't expect this connection, so we close it
			silentlyClose(key);
			return;
		}
		LOGGER.info("Successfully connected privately to user " + pending.getRecipient());
		state.pendingConnections().remove(privateClientSocket);
		key.interestOps(OP_READ);
		var recipientHandle = configurePrivateClient(pending.getRecipient(), pending.getAuthKey(), key);
		var queue = state.outgoingPrivateRequests().getOrDefault(pending.getRecipient(), new ArrayDeque<>());
		while (!queue.isEmpty()) {
			recipientHandle.getNetworkContext().queueFrame(queue.remove().apply(pending.getAuthKey()));
		}
	}
	
	private PrivateClientHandle configurePrivateClient(String recipient, ByteBuffer authKey, SelectionKey key) {
		var handle = new PrivateClientHandle(state, recipient, authKey);
		var networkContext = new NetworkContext(key, new ChatHackFrameReader(), handle, false);
		if (recipient != null) {
			networkContext.setCloseHandler(() -> state.closePrivateConnection(recipient));
			state.activePrivateConnections().put(recipient, handle);
		}
		handle.setNetworkContext(networkContext);
		key.attach(networkContext);
		return handle;
	}

	private static void silentlyClose(SelectionKey key) {
		try {
			key.channel().close();
		} catch (IOException e) {
			// ignore exception
		}
	}
	
	public static void main(String[] args) throws IOException {
		if (args.length < 4) {
			usage();
			System.exit(1);
			return;
		}
		System.setProperty("java.util.logging.SimpleFormatter.format",
	              "%4$s: %5$s %n");
		var client = create(args[0], Integer.parseInt(args[1]), args[2], args[3], args.length >= 5 ? args[4] : null);
		client.launch();
	}

	private static void usage() {
		System.out.println("Usage : " + Client.class.getName() + " host port file_directory username [password]");
	}
}
