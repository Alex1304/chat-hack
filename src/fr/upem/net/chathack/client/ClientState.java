package fr.upem.net.chathack.client;

import static java.nio.channels.SelectionKey.OP_CONNECT;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Optional;
import java.util.Queue;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.upem.net.chathack.protocol.frame.Frame;
import fr.upem.net.chathack.protocol.frame.PrivateRequestFrame;

/**
 * Manages the state of the client.
 */
public class ClientState {

	private static final Logger LOGGER = Logger.getLogger(ClientState.class.getName());
	
	private final Selector selector;
	private final Path fileDirectory;
	private final int privateServerPort;
	private final String username;
	private final String password; // may be null
	
	// Private Connections
	private final HashMap<String, Queue<Function<ByteBuffer, Frame>>> outgoingPrivateRequests = new HashMap<>();
	private final HashMap<ByteBuffer, String> acceptedKeys = new HashMap<>(); 
	private final HashMap<SocketChannel, PendingPrivateConnection> pendingConnections = new HashMap<>();
	private final HashMap<String, PrivateClientHandle> activePrivateConnections = new HashMap<>();
	
	private ChathackServerHandle serverHandle;

	public ClientState(Selector selector, Path fileDirectory, int privateServerPort, String username, String password) {
		this.selector = selector;
		this.fileDirectory = fileDirectory;
		this.privateServerPort = privateServerPort;
		this.username = username;
		this.password = password;
	}

	public void connectPrivately(String recipient, ByteBuffer authKey, Inet4Address ip, int port) {
		try {
			var address = new InetSocketAddress(ip, port);
			SocketChannel privateClientSocket = SocketChannel.open();
			privateClientSocket.configureBlocking(false);
			privateClientSocket.register(selector, OP_CONNECT);
			privateClientSocket.connect(address);
			pendingConnections.put(privateClientSocket, new PendingPrivateConnection(recipient, authKey));
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Could not initiate private connection with " + recipient, e);
		}
	}
	
	public void withServerHandle(Consumer<ChathackServerHandle> action) {
		if (serverHandle != null) {
			action.accept(serverHandle);
		} else {
			throw new IllegalStateException("Not yet connected to chathack server");
		}
	}
	
	public void setServerHandle(ChathackServerHandle serverHandle) {
		this.serverHandle = serverHandle;
	}

	public String getUsername() {
		return username;
	}

	public Optional<String> getPassword() {
		return Optional.ofNullable(password);
	}
	
	public int getPrivateServerPort() {
		return privateServerPort;
	}
	
	/**
	 * Attempts to send a private frame with an existing private connection. If no
	 * private connection exists, the frame is queued and a private request will be
	 * sent.
	 * 
	 * @param recipient   the recipient username
	 * @param frameGetter a function returning the frame to send for a given private
	 *                    connection key
	 */
	public void queuePrivateFrame(String recipient, Function<ByteBuffer, Frame> frameGetter) {
		var recipientHandle = activePrivateConnections.get(recipient);
		if (recipientHandle != null) {
			recipientHandle.getNetworkContext().queueFrame(frameGetter.apply(recipientHandle.getAuthKey()));
		} else {
			outgoingPrivateRequests.computeIfAbsent(recipient, k -> new ArrayDeque<>()).add(frameGetter);
			serverHandle.getNetworkContext().queueFrame(new PrivateRequestFrame(recipient));
			LOGGER.info("A private connection request has been sent to " + recipient
					+ ". Your message will be delivered upon acceptation.");
		}
	}
	
	public void addAcceptedKey(ByteBuffer authKey, String username) {
		acceptedKeys.put(authKey, username);
	}
	
	public void assignKey(PrivateClientHandle privateClientHandle, ByteBuffer key) {
		var username = acceptedKeys.remove(key);
		if (username == null) {
			return;
		}
		privateClientHandle.setAuthKey(key);
		privateClientHandle.setRecipient(username);
		activePrivateConnections().put(username, privateClientHandle);
		privateClientHandle.getNetworkContext().setCloseHandler(() -> closePrivateConnection(username));
	}
	
	public void saveFile(String filename, ByteBuffer data) {
		try (var fc = FileChannel.open(fileDirectory.resolve(filename), CREATE, TRUNCATE_EXISTING, WRITE)) {
			fc.write(data);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Could not save file " + filename, e);
		}
	}
	
	public Optional<ByteBuffer> readFile(String filename) {
		var path = fileDirectory.resolve(filename);
		try (var fc = FileChannel.open(path, READ)) {
			var bb = ByteBuffer.allocate(1024);
			while (bb.remaining() == fc.read(bb)) {
				var newBb = ByteBuffer.allocate(bb.capacity() * 2);
				newBb.put(bb.flip());
				bb = newBb;
			}
			return Optional.of(bb.flip());
		} catch (NoSuchFileException e) {
			LOGGER.warning("File " + path + " not found");
			return Optional.empty();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Could not read file " + path, e);
			return Optional.empty();
		}
	}
	
	public HashMap<String, Queue<Function<ByteBuffer, Frame>>> outgoingPrivateRequests() {
		return outgoingPrivateRequests;
	}

	public HashMap<ByteBuffer, String> unassignedAuthKeys() {
		return acceptedKeys;
	}

	public HashMap<SocketChannel, PendingPrivateConnection> pendingConnections() {
		return pendingConnections;
	}

	public HashMap<String, PrivateClientHandle> activePrivateConnections() {
		return activePrivateConnections;
	}
	
	public void closePrivateConnection(String username) {
		var handle = activePrivateConnections.remove(username);
		if (handle != null) {
			LOGGER.warning(username + " has closed the private connection with you");
		}
	}
}
