package fr.upem.net.chathack.client;


import java.nio.ByteBuffer;

import fr.upem.net.chathack.protocol.NetworkContext;
import fr.upem.net.chathack.protocol.frame.PrivateClientFrameVisitor;
import fr.upem.net.chathack.protocol.frame.PrivateFileSendingFrame;
import fr.upem.net.chathack.protocol.frame.PrivateMessageSendingFrame;

/**
 * Handle that represents a client connected privately.
 */
public class PrivateClientHandle implements PrivateClientFrameVisitor {
	
	private final ClientState state;
	private String recipient;
	private ByteBuffer authKey;
	private NetworkContext networkContext;

	public PrivateClientHandle(ClientState state, String recipient, ByteBuffer authKey) {
		this.state = state;
		this.recipient = recipient;
		this.authKey = authKey;
	}

	public NetworkContext getNetworkContext() {
		return networkContext;
	}

	public void setNetworkContext(NetworkContext networkContext) {
		this.networkContext = networkContext;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public ByteBuffer getAuthKey() {
		return authKey;
	}

	public void setAuthKey(ByteBuffer key) {
		this.authKey = key;
	}
	
	@Override
	public void visit(PrivateFileSendingFrame frame) {
		validateKey(frame.getKey());
		System.out.println("[PRIVATE] " + recipient + " sent you " + frame.getFilename()
				+ " (" + frame.getData().remaining() + " bytes)");
		state.saveFile(frame.getFilename(), frame.getData());
	}

	@Override
	public void visit(PrivateMessageSendingFrame frame) {
		validateKey(frame.getKey());
		System.out.println("[PRIVATE] " + recipient + ": " + frame.getMessage());
	}
	
	private void validateKey(ByteBuffer receivedKey) {
		if (authKey == null) {
			state.assignKey(this, receivedKey);
		}
		if (authKey == null || !receivedKey.equals(authKey)) {
			throw new RuntimeException("Incorrect key");
		}
	}
}
