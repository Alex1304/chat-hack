package fr.upem.net.chathack.client;

/**
 * Listener for user input.
 */
public interface UserInputListener {

	/**
	 * Callback invoked when user input is received.
	 * 
	 * @param input the input string
	 */
	void onUserInput(String input);
}
