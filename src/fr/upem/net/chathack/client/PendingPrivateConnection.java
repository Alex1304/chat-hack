package fr.upem.net.chathack.client;

import java.nio.ByteBuffer;

/**
 * Data class for a pending private connection.
 */
public class PendingPrivateConnection {
	
	private final String recipient;
	private final ByteBuffer authKey;
	
	public PendingPrivateConnection(String recipient, ByteBuffer authKey) {
		this.recipient = recipient;
		this.authKey = authKey;
	}

	public String getRecipient() {
		return recipient;
	}

	public ByteBuffer getAuthKey() {
		return authKey;
	}
}