package fr.upem.net.chathack.client;

import static fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame.PASSWORD_INVALID;
import static fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame.USERNAME_TAKEN;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.logging.Logger;

import fr.upem.net.chathack.protocol.NetworkContext;
import fr.upem.net.chathack.protocol.frame.ClientFrameVisitor;
import fr.upem.net.chathack.protocol.frame.ConnectionApprovalFrame;
import fr.upem.net.chathack.protocol.frame.ConnectionAuthenticatedFrame;
import fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame;
import fr.upem.net.chathack.protocol.frame.ConnectionUnauthenticatedFrame;
import fr.upem.net.chathack.protocol.frame.PrivateApprovalFrame;
import fr.upem.net.chathack.protocol.frame.PrivateDenialFrame;
import fr.upem.net.chathack.protocol.frame.PrivateFileSendingFrame;
import fr.upem.net.chathack.protocol.frame.PrivateMessageSendingFrame;
import fr.upem.net.chathack.protocol.frame.PrivateApprovalTransmissionFrame;
import fr.upem.net.chathack.protocol.frame.PrivateDenialTransmissionFrame;
import fr.upem.net.chathack.protocol.frame.PrivateRequestErrorFrame;
import fr.upem.net.chathack.protocol.frame.PrivateKeyFrame;
import fr.upem.net.chathack.protocol.frame.PrivateRequestTransmissionFrame;
import fr.upem.net.chathack.protocol.frame.PublicMessageBroadcastingFrame;
import fr.upem.net.chathack.protocol.frame.PublicMessageSendingFrame;

/**
 * Handle that represents the chat hack server.
 */
public class ChathackServerHandle implements ClientFrameVisitor, UserInputListener {

	private static final Logger LOGGER = Logger.getLogger(ChathackServerHandle.class.getName());
	
	private final ClientState state;
	private NetworkContext networkContext;
	
	private final HashSet<String> receivedPrivateRequests = new HashSet<>();
	private final Map<String, Consumer<String>> commandMap = initCommandMap();
	
	public ChathackServerHandle(ClientState state) {
		this.state = state;
	}

	public NetworkContext getNetworkContext() {
		return networkContext;
	}

	public void setNetworkContext(NetworkContext context) {
		this.networkContext = context;
	}
	
	public void initConnection() {
		state.getPassword().ifPresentOrElse(password -> {
			networkContext.queueFrame(new ConnectionAuthenticatedFrame(state.getUsername(), password));
		}, () -> {
			networkContext.queueFrame(new ConnectionUnauthenticatedFrame(state.getUsername()));
		});
	}
	
	@Override
	public void visit(ConnectionApprovalFrame frame) {
		LOGGER.info("Welcome to Chat Hack, " + state.getUsername() + "!");
	}

	@Override
	public void visit(ConnectionDenialFrame frame) {
		String message;
		switch (frame.getErrorCode()) {
			case USERNAME_TAKEN:
				message = "Username is taken";
				break;
			case PASSWORD_INVALID:
				message = "Password is incorrect";
				break;
			default:
				throw new AssertionError();
		}
		LOGGER.severe("Failed to authenticate to server: " + message);
	}

	@Override
	public void visit(PrivateApprovalTransmissionFrame frame) {
		LOGGER.info("User " + frame.getRecipient() + " has accepted your private connection request.");
		state.connectPrivately(frame.getRecipient(), frame.getKey(), frame.getIp(), frame.getPort());
	}

	@Override
	public void visit(PrivateDenialTransmissionFrame frame) {
		LOGGER.info("User " + frame.getRecipient() + " has rejected your private connection request.");
		state.outgoingPrivateRequests().remove(frame.getRecipient());
	}

	@Override
	public void visit(PrivateRequestErrorFrame frame) {
		LOGGER.warning("Failed to send private connection request to " + frame.getRecipient() + ": they are probably not online.");
	}

	@Override
	public void visit(PrivateKeyFrame frame) {
		state.addAcceptedKey(frame.getKey(), frame.getRecipient());
	}

	@Override
	public void visit(PrivateRequestTransmissionFrame frame) {
		receivedPrivateRequests.add(frame.getSender());
		LOGGER.info(frame.getSender() + " wants to send you a private message. Use \"!accept " + frame.getSender() + "\" to start the communication.");
	}

	@Override
	public void visit(PublicMessageBroadcastingFrame frame) {
		System.out.println("[PUBLIC] " + frame.getAuthor() + ": " + frame.getMessage());
	}
	
	@Override
	public void onUserInput(String input) {
		if (input.isEmpty()) {
			return;
		}
		var tokens = input.split(" ", 2);
		var prefix = tokens[0].charAt(0);
		var keyword = tokens[0].substring(1);
		var content = tokens.length > 1 ? tokens[1] : "";
		switch (prefix) {
			case '@': // keyword = username
				state.queuePrivateFrame(keyword, key -> new PrivateMessageSendingFrame(key, content));
				break;
			case '/': // keyword = username
				state.readFile(content).ifPresent(data -> state.queuePrivateFrame(keyword, key -> new PrivateFileSendingFrame(key, content, data)));
				break;
			case '!': // keyword = command name
				Optional.ofNullable(commandMap.get(keyword)).ifPresentOrElse(
						action -> action.accept(content),
						() -> LOGGER.warning("Unknown command"));
				break;
			default:
				networkContext.queueFrame(new PublicMessageSendingFrame(input));
		}
	}
	
	private Map<String, Consumer<String>> initCommandMap() {
		return Map.ofEntries(
				Map.entry("accept", content -> {
					if (receivedPrivateRequests.remove(content)) {
						networkContext.queueFrame(new PrivateApprovalFrame(content, state.getPrivateServerPort()));
					}
				}),
				Map.entry("reject", content -> {
					if (receivedPrivateRequests.remove(content)) {
						networkContext.queueFrame(new PrivateDenialFrame(content));
					}
				}),
				Map.entry("close", content -> {
					var handle = state.activePrivateConnections().remove(content);
					if (handle == null) {
						LOGGER.warning("No connection with " + content + " is open");
					} else {
						handle.getNetworkContext().close();
						LOGGER.info("Closed private connection with " + content);
					}
				}));
	}

}
