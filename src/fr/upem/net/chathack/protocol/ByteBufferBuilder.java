package fr.upem.net.chathack.protocol;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * Convenient class to build byte buffers.
 */
public final class ByteBufferBuilder {

	private final ArrayList<Consumer<ByteBuffer>> steps = new ArrayList<>();
	private int size;

	/**
	 * Appends another ByteBuffer. The given ByteBuffer must be in read mode.
	 * 
	 * @param other the other ByteBuffer
	 * @return this builder
	 */
	public ByteBufferBuilder append(ByteBuffer other) {
		size += other.remaining();
		steps.add(buf -> buf.put(other.duplicate()));
		return this;
	}
	
	/**
	 * Appends the size of the given ByteBuffer followed by its data. The given
	 * ByteBuffer must be in read mode.
	 * 
	 * @param other the other ByteBuffer
	 * @return this builder
	 */
	public ByteBufferBuilder appendWithSize(ByteBuffer other) {
		size += Integer.BYTES + other.remaining();
		steps.add(buf -> {
			buf.putInt(other.remaining());
			buf.put(other.duplicate());
		});
		return this;
	}

	/**
	 * Appends a byte value.
	 * 
	 * @param value the byte value
	 * @return this builder
	 */
	public ByteBufferBuilder appendByte(byte value) {
		size += Byte.BYTES;
		steps.add(buf -> buf.put(value));
		return this;
	}

	/**
	 * Appends an int value.
	 * 
	 * @param value the int value
	 * @return this builder
	 */
	public ByteBufferBuilder appendInt(int value) {
		size += Integer.BYTES;
		steps.add(buf -> buf.putInt(value));
		return this;
	}

	/**
	 * Appends a long value.
	 * 
	 * @param value the long value
	 * @return this builder
	 */
	public ByteBufferBuilder appendLong(long value) {
		size += Long.BYTES;
		steps.add(buf -> buf.putLong(value));
		return this;
	}

	/**
	 * Appends a String value encoded in UTF-8
	 * 
	 * @param value the String value
	 * @return this builder
	 */
	public ByteBufferBuilder appendStringUTF8(String value) {
		appendWithSize(StandardCharsets.UTF_8.encode(value));
		return this;
	}

	/**
	 * Creates the ByteBuffer containing encoded elements appended in order. The
	 * capacity of the ByteBuffer is equal to the minimum capacity required to store
	 * all appended elements, and is returned in read-mode.
	 * 
	 * @return the built ByteBuffer
	 */
	public ByteBuffer build() {
		var buf = ByteBuffer.allocate(size);
		steps.forEach(step -> step.accept(buf));
		return buf.flip();
	}
}
