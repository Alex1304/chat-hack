package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;

public class IntReader extends AbstractReader<Integer> {
	
	public IntReader() {
		setKnownSize(Integer.BYTES);
	}

	@Override
	Integer decode(ByteBuffer buf) throws DecodeException {
		return buf.getInt();
	}

}
