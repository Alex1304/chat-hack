package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;

import fr.upem.net.chathack.protocol.frame.PrivateKeyFrame;

public class PrivateKeyFrameReader extends AbstractFrameReader<PrivateKeyFrame> {
	
	private String recipient;
	private ByteBuffer key;
	
	public PrivateKeyFrameReader() {
		expectValue(new StringReader(), v -> this.recipient = v);
		expectValue(new BinaryReader(16), v -> this.key = v);
	}

	@Override
	PrivateKeyFrame get() {
		return new PrivateKeyFrame(recipient, key);
	}

}
