package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.AuthResponseFrame;

/**
 * Reader implementation capable of decoding frames from the auth server.
 */
public class AuthResponseFrameReader extends AbstractFrameReader<AuthResponseFrame> {

	private byte response;
	private long id;

	public AuthResponseFrameReader() {
		expectValue(new ByteReader(), v -> this.response = v);
		expectValue(new LongReader(), v -> this.id = v);
	}

	@Override
	AuthResponseFrame get() {
		return new AuthResponseFrame(response, id);
	}
}