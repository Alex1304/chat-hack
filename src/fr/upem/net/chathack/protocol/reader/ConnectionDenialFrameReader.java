package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame;

public class ConnectionDenialFrameReader extends AbstractFrameReader<ConnectionDenialFrame> {
	
	private byte errorCode;
	
	public ConnectionDenialFrameReader() {
		expectValue(new ByteReader(), v -> this.errorCode = DecodeException.validateConnectionDenialErrorCode(v));
	}

	@Override
	ConnectionDenialFrame get() {
		return new ConnectionDenialFrame(errorCode);
	}

}
