package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;

import fr.upem.net.chathack.protocol.frame.PrivateMessageSendingFrame;

public class PrivateMessageSendingFrameReader extends AbstractFrameReader<PrivateMessageSendingFrame> {
	
	private ByteBuffer key;
	private String message;
	
	public PrivateMessageSendingFrameReader() {
		expectValue(new BinaryReader(16), v -> this.key = v);
		expectValue(new StringReader(), v -> this.message = v);
	}

	@Override
	PrivateMessageSendingFrame get() {
		return new PrivateMessageSendingFrame(key, message);
	}

}
