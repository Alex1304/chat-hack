package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.ConnectionApprovalFrame;

public class ConnectionApprovalFrameReader extends AbstractFrameReader<ConnectionApprovalFrame> {

	@Override
	ConnectionApprovalFrame get() {
		return new ConnectionApprovalFrame();
	}
}
