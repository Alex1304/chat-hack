package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;
import java.util.Optional;

/**
 * Stateful value reader from ByteBuffers
 * 
 * @param <R> the type of value to decode
 */
public interface Reader<R> {

	/**
	 * Attempts to read a value from the given ByteBuffer. If more data is needed in
	 * order to construct the value, it will return an empty Optional. If the read
	 * data can not be decoded, it will throw {@link DecodeException}.
	 * 
	 * @param buf the ByteBuffer where data is extracted from
	 * @return the decoded value, if present
	 * @throws DecodeException if the data could not be decoded
	 */
	Optional<R> read(ByteBuffer buf) throws DecodeException;
}
