package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.PrivateRequestErrorFrame;

public class PrivateRequestErrorFrameReader extends AbstractFrameReader<PrivateRequestErrorFrame> {
	
	private String recipient;
	
	public PrivateRequestErrorFrameReader() {
		expectValue(new StringReader(), v -> this.recipient = v);
	}

	@Override
	PrivateRequestErrorFrame get() {
		return new PrivateRequestErrorFrame(recipient);
	}

}
