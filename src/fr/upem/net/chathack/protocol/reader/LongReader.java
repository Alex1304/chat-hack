package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;

public class LongReader extends AbstractReader<Long> {
	
	public LongReader() {
		setKnownSize(Long.BYTES);
	}

	@Override
	Long decode(ByteBuffer buf) throws DecodeException {
		return buf.getLong();
	}

}
