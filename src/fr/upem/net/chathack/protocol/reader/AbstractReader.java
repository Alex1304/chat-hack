package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;
import java.util.Optional;

import fr.upem.net.chathack.protocol.ByteBufferBuilder;

abstract class AbstractReader<R> implements Reader<R> {

	private int knownSize = -1;
	private boolean hasReadSize;
	private int bytesExpectedToReadSize = Integer.BYTES;
	private int bytesExpectedToDecodeData;
	private ByteBufferBuilder accumulator = new ByteBufferBuilder();
	
	@Override
	public Optional<R> read(ByteBuffer buf) throws DecodeException {
		if (!hasReadSize) {
			while (buf.hasRemaining() && bytesExpectedToReadSize > 0) {
				bytesExpectedToReadSize--;
				accumulator.appendByte(buf.get());
			}
			if (bytesExpectedToReadSize == 0) {
				readSize();
			} else {
				return Optional.empty();
			}
		}
		while (buf.hasRemaining() && bytesExpectedToDecodeData > 0) {
			bytesExpectedToDecodeData--;
			accumulator.appendByte(buf.get());
		}
		if (bytesExpectedToDecodeData == 0) {
			try {
				return Optional.of(decode(accumulator.build()));
			} finally {
				reset();
			}
		}
		return Optional.empty();
	}
	
	private void reset() {
		hasReadSize = false;
		bytesExpectedToReadSize = Integer.BYTES;
		bytesExpectedToDecodeData = 0;
		accumulator = new ByteBufferBuilder();
		if (knownSize >= 0) {
			setKnownSize(knownSize);
		}
	}
	
	void readSize() {
		hasReadSize = true;
		bytesExpectedToDecodeData = accumulator.build().getInt();
		accumulator = new ByteBufferBuilder();
	}
	
	void setKnownSize(int size) {
		knownSize = size;
		hasReadSize = true;
		bytesExpectedToReadSize = 0;
		bytesExpectedToDecodeData = size;
	}
	
	abstract R decode(ByteBuffer buf) throws DecodeException;
}
