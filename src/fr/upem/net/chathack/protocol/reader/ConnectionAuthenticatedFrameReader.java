package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.ConnectionAuthenticatedFrame;

public class ConnectionAuthenticatedFrameReader extends AbstractFrameReader<ConnectionAuthenticatedFrame> {
	
	private String username;
	private String password;
	
	public ConnectionAuthenticatedFrameReader() {
		expectValue(new StringReader(), v -> this.username = v);
		expectValue(new StringReader(), v -> this.password = v);
	}

	@Override
	ConnectionAuthenticatedFrame get() {
		return new ConnectionAuthenticatedFrame(username, password);
	}

}
