package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Queue;

import fr.upem.net.chathack.protocol.frame.Frame;

abstract class AbstractFrameReader<R extends Frame> implements Reader<R> {

	private final ArrayList<ExpectedValue<?>> expectedValues = new ArrayList<>();
	private Queue<ExpectedValue<?>> queue = new ArrayDeque<>();
	private ExpectedValue<?> current;
	
	@Override
	public Optional<R> read(ByteBuffer buf) throws DecodeException {
		// First time reading: fill the queue and make first element the current, return
		// value immediately if empty
		if (current == null) {
			queue.addAll(expectedValues);
			if (queue.isEmpty()) {
				try {
					return Optional.of(get());
				} finally {
					reset();
				}
			}
			current = queue.remove();
		}
		while (buf.hasRemaining()) {
			// Perform read, reset if error
			try {
				current.readAndConsumeValueIfPresent(buf);
			} catch (DecodeException e) {
				reset();
				throw e;
			}
			// If current is done, move to next, or return value if empty
			if (current.done) {
				if (queue.isEmpty()) {
					try {
						return Optional.of(get());
					} finally {
						reset();
					}
				}
				current = queue.remove();
			}
		}
		return Optional.empty();
	}
	
	private void reset() {
		queue = new ArrayDeque<>();
		current = null;
	}
	
	<T> void expectValue(Reader<T> valueReader, ValueConsumer<T> valueConsumer) {
		expectedValues.add(new ExpectedValue<>(valueReader, valueConsumer));
	}
	
	abstract R get();
	
	private static class ExpectedValue<V> {
		private final Reader<V> valueReader;
		private final ValueConsumer<V> valueConsumer;
		private boolean done;
		
		ExpectedValue(Reader<V> valueReader, ValueConsumer<V> valueConsumer) {
			this.valueReader = valueReader;
			this.valueConsumer = valueConsumer;
		}
		
		void readAndConsumeValueIfPresent(ByteBuffer buf) throws DecodeException {
			var value = valueReader.read(buf);
			if (value.isPresent()) {
				valueConsumer.accept(value.orElseThrow());
				done = true;
			}
		}
	}
	
	static interface ValueConsumer<V> {
		void accept(V t) throws DecodeException;
	}
}
