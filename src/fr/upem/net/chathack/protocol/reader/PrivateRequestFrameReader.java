package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.PrivateRequestFrame;

public class PrivateRequestFrameReader extends AbstractFrameReader<PrivateRequestFrame> {
	
	private String recipient;
	
	public PrivateRequestFrameReader() {
		expectValue(new StringReader(), v -> this.recipient = v);
	}

	@Override
	PrivateRequestFrame get() {
		return new PrivateRequestFrame(recipient);
	}

}
