package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.PublicMessageBroadcastingFrame;

public class PublicMessageBroadcastingFrameReader extends AbstractFrameReader<PublicMessageBroadcastingFrame> {
	
	private String author;
	private String message;
	
	public PublicMessageBroadcastingFrameReader() {
		expectValue(new StringReader(), v -> this.author = v);
		expectValue(new StringReader(), v -> this.message = v);
	}

	@Override
	PublicMessageBroadcastingFrame get() {
		return new PublicMessageBroadcastingFrame(author, message);
	}

}
