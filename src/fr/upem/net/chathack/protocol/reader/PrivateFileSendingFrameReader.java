package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;

import fr.upem.net.chathack.protocol.frame.PrivateFileSendingFrame;

public class PrivateFileSendingFrameReader extends AbstractFrameReader<PrivateFileSendingFrame> {
	
	private ByteBuffer key;
	private String filename;
	private ByteBuffer data;
	
	public PrivateFileSendingFrameReader() {
		expectValue(new BinaryReader(16), v -> this.key = v);
		expectValue(new StringReader(), v -> this.filename = v);
		expectValue(new BinaryReader(), v -> this.data = v);
	}

	@Override
	PrivateFileSendingFrame get() {
		return new PrivateFileSendingFrame(key, filename, data);
	}

}
