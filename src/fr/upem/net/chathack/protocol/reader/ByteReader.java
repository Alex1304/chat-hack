package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;

public class ByteReader extends AbstractReader<Byte> {
	
	public ByteReader() {
		setKnownSize(Byte.BYTES);
	}

	@Override
	Byte decode(ByteBuffer buf) throws DecodeException {
		return buf.get();
	}

}
