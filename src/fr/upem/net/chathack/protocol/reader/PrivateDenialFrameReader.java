package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.PrivateDenialFrame;

public class PrivateDenialFrameReader extends AbstractFrameReader<PrivateDenialFrame> {
	
	private String sender;
	
	public PrivateDenialFrameReader() {
		expectValue(new StringReader(), v -> this.sender = v);
	}

	@Override
	PrivateDenialFrame get() {
		return new PrivateDenialFrame(sender);
	}

}
