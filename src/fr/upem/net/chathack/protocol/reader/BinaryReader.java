package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;

public class BinaryReader extends AbstractReader<ByteBuffer> {
	
	/**
	 * Creates a reader that reads raw binary data. It will read the size in the
	 * buffer.
	 */
	public BinaryReader() {
		this(-1);
	}
	
	/**
	 * Creates a reader that reads raw binary data.
	 * 
	 * @param size the size of data to read, negative means size is unknown and will
	 *             be read from the buffer
	 */
	public BinaryReader(int size) {
		if (size >= 0) {
			setKnownSize(size);
		}
	}

	@Override
	ByteBuffer decode(ByteBuffer buf) throws DecodeException {
		return buf;
	}

}
