package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class StringReader extends AbstractReader<String> {

	@Override
	String decode(ByteBuffer buf) throws DecodeException {
		return StandardCharsets.UTF_8.decode(buf).toString();
	}

}
