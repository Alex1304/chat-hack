package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.ConnectionUnauthenticatedFrame;

public class ConnectionUnauthenticatedFrameReader extends AbstractFrameReader<ConnectionUnauthenticatedFrame> {
	
	private String username;
	
	public ConnectionUnauthenticatedFrameReader() {
		expectValue(new StringReader(), v -> this.username = v);
	}

	@Override
	ConnectionUnauthenticatedFrame get() {
		return new ConnectionUnauthenticatedFrame(username);
	}

}
