package fr.upem.net.chathack.protocol.reader;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Optional;

import fr.upem.net.chathack.protocol.Opcodes;
import fr.upem.net.chathack.protocol.frame.Frame;

/**
 * Reader implementation capable of decoding frames for the Chat Hack protocol.
 */
public class ChatHackFrameReader implements Reader<Frame> {
	
	private static final Map<Byte, Reader<? extends Frame>> FRAME_READERS = initFrameReaders();

	private Reader<? extends Frame> matchingReader;
	
	@Override
	public Optional<Frame> read(ByteBuffer buf) throws DecodeException {
		if (matchingReader == null) {
			if (!buf.hasRemaining()) {
				return Optional.empty();
			}
			var opcode = buf.get();
			if (!Opcodes.isValid(opcode)) {
				throw new DecodeException("Unknown opcode: " + opcode);
			}
			matchingReader = FRAME_READERS.get(opcode);
		}
		var readResult = matchingReader.read(buf).map(Frame.class::cast);
		readResult.ifPresent(frame -> matchingReader = null);
		return readResult;
	}
	
	private static Map<Byte, Reader<? extends Frame>> initFrameReaders() {
		return Map.ofEntries(
				Map.entry(Opcodes.CRA, new ConnectionApprovalFrameReader()),
				Map.entry(Opcodes.CNP, new ConnectionAuthenticatedFrameReader()),
				Map.entry(Opcodes.CRD, new ConnectionDenialFrameReader()),
				Map.entry(Opcodes.CNU, new ConnectionUnauthenticatedFrameReader()),
				Map.entry(Opcodes.PVA, new PrivateApprovalFrameReader()),
				Map.entry(Opcodes.PVD, new PrivateDenialFrameReader()),
				Map.entry(Opcodes.PVF, new PrivateFileSendingFrameReader()),
				Map.entry(Opcodes.PVM, new PrivateMessageSendingFrameReader()),
				Map.entry(Opcodes.PVAT, new PrivateApprovalTransmissionFrameReader()),
				Map.entry(Opcodes.PVDT, new PrivateDenialTransmissionFrameReader()),
				Map.entry(Opcodes.PVRE, new PrivateRequestErrorFrameReader()),
				Map.entry(Opcodes.PVR, new PrivateRequestFrameReader()),
				Map.entry(Opcodes.PVRT, new PrivateRequestTransmissionFrameReader()),
				Map.entry(Opcodes.PVK, new PrivateKeyFrameReader()),
				Map.entry(Opcodes.PBB, new PublicMessageBroadcastingFrameReader()),
				Map.entry(Opcodes.PBS, new PublicMessageSendingFrameReader()));
	}

}
