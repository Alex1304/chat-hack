package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame;

public class DecodeException extends Exception {
	private static final long serialVersionUID = 4214654982199041954L;

	DecodeException() {
		super();
	}

	DecodeException(String message, Throwable cause) {
		super(message, cause);
	}

	DecodeException(String message) {
		super(message);
	}

	DecodeException(Throwable cause) {
		super(cause);
	}
	
	public static int validatePort(int port) throws DecodeException {
		if (port < 0 || port > 65535) {
			throw new DecodeException("Invalid port number: " + port);
		}
		return port;
	}

	public static byte validateConnectionDenialErrorCode(byte errorCode) throws DecodeException {
		if (errorCode < ConnectionDenialFrame.USERNAME_TAKEN || errorCode > ConnectionDenialFrame.PASSWORD_INVALID) {
			throw new DecodeException("Unknown error code: " + errorCode);
		}
		return errorCode;
	}
}
