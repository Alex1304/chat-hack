package fr.upem.net.chathack.protocol.reader;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import fr.upem.net.chathack.protocol.frame.PrivateApprovalTransmissionFrame;

public class PrivateApprovalTransmissionFrameReader extends AbstractFrameReader<PrivateApprovalTransmissionFrame> {
	
	private String recipient;
	private ByteBuffer key;
	private Inet4Address ip;
	private int port;

	public PrivateApprovalTransmissionFrameReader() {
		expectValue(new StringReader(), v -> this.recipient = v);
		expectValue(new BinaryReader(16), v -> this.key = v);
		expectValue(new BinaryReader(4), v -> {
			try {
				this.ip = (Inet4Address) Inet4Address.getByAddress(v.array());
			} catch (UnknownHostException e) {
				 // We know that byte array is correct length so this should never happen
				throw new AssertionError();
			}
		});
		expectValue(new IntReader(), v -> this.port = DecodeException.validatePort(v));
	}

	@Override
	PrivateApprovalTransmissionFrame get() {
		return new PrivateApprovalTransmissionFrame(recipient, key, ip, port);
	}

}
