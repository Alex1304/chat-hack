package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.PrivateDenialTransmissionFrame;

public class PrivateDenialTransmissionFrameReader extends AbstractFrameReader<PrivateDenialTransmissionFrame> {
	
	private String recipient;
	
	public PrivateDenialTransmissionFrameReader() {
		expectValue(new StringReader(), v -> this.recipient = v);
	}

	@Override
	PrivateDenialTransmissionFrame get() {
		return new PrivateDenialTransmissionFrame(recipient);
	}

}
