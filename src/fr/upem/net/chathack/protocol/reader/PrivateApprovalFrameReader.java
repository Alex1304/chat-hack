package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.PrivateApprovalFrame;

public class PrivateApprovalFrameReader extends AbstractFrameReader<PrivateApprovalFrame> {
	
	private String sender;
	private int port;
	
	public PrivateApprovalFrameReader() {
		expectValue(new StringReader(), v -> this.sender = v);
		expectValue(new IntReader(), v -> this.port = DecodeException.validatePort(v));
	}

	@Override
	PrivateApprovalFrame get() {
		return new PrivateApprovalFrame(sender, port);
	}

}
