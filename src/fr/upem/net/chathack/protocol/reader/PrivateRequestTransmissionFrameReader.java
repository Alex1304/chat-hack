package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.PrivateRequestTransmissionFrame;

public class PrivateRequestTransmissionFrameReader extends AbstractFrameReader<PrivateRequestTransmissionFrame> {
	
	private String sender;
	
	public PrivateRequestTransmissionFrameReader() {
		expectValue(new StringReader(), v -> this.sender = v);
	}

	@Override
	PrivateRequestTransmissionFrame get() {
		return new PrivateRequestTransmissionFrame(sender);
	}

}
