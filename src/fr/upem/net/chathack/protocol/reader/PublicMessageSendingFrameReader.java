package fr.upem.net.chathack.protocol.reader;

import fr.upem.net.chathack.protocol.frame.PublicMessageSendingFrame;

public class PublicMessageSendingFrameReader extends AbstractFrameReader<PublicMessageSendingFrame> {
	
	private String message;
	
	public PublicMessageSendingFrameReader() {
		expectValue(new StringReader(), v -> this.message = v);
	}

	@Override
	PublicMessageSendingFrame get() {
		return new PublicMessageSendingFrame(message);
	}

}
