package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.ByteBufferBuilder;

public class AuthRequestLoginFrame implements Frame {
	
	private final long id;
	private final String username;
	private final String password;
	
	public AuthRequestLoginFrame(long id, String username, String password) {
		this.id = requireNonNull(id);
		this.username = requireNonNull(username);
		this.password = requireNonNull(password);
	}

	@Override
	public ByteBuffer encode() {
		return new ByteBufferBuilder()
				.appendByte((byte)1)
				.appendLong(id)
				.appendStringUTF8(username)
				.appendStringUTF8(password)
				.build();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(id, username, password);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof AuthRequestLoginFrame))
			return false;
		AuthRequestLoginFrame other = (AuthRequestLoginFrame) obj;
		return Objects.equals(id, other.id)
				&& Objects.equals(username, other.username)
				&& Objects.equals(password, other.password);
	}
	
	@Override
	public String toString() {
		return "AuthRequestLoginFrame{id=" + id + ","
				+ ", username=" + username
				+ ", password=" + password
				+ "}";
	}

	public long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public void accept(FrameVisitor visitor) {
		return;
	}
}
