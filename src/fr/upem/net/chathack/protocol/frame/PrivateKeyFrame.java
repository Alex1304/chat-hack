package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PrivateKeyFrame extends AbstractFrame {

	private final String recipient;
	private final ByteBuffer key;

	public PrivateKeyFrame(String recipient, ByteBuffer key) {
		super(Opcodes.PVK);
		this.recipient = requireNonNull(recipient);
		this.key = requireNonNull(key);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(recipient)
				.append(key)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(key, recipient);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PrivateKeyFrame))
			return false;
		PrivateKeyFrame other = (PrivateKeyFrame) obj;
		return Objects.equals(key, other.key) && Objects.equals(recipient, other.recipient);
	}

	@Override
	public String toString() {
		return "PrivateKeyFrame{opcode=" + opcode() + ", recipient=" + recipient + ", key="
				+ byteBufferToString(key) + "}";
	}

	public String getRecipient() {
		return recipient;
	}

	public ByteBuffer getKey() {
		return key;
	}
	
}
