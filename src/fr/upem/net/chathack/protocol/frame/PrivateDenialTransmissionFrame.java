package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PrivateDenialTransmissionFrame extends AbstractFrame {

	private final String recipient;

	public PrivateDenialTransmissionFrame(String recipient) {
		super(Opcodes.PVDT);
		this.recipient = requireNonNull(recipient);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(recipient)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	public String getRecipient() {
		return recipient;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(recipient);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PrivateDenialTransmissionFrame))
			return false;
		PrivateDenialTransmissionFrame other = (PrivateDenialTransmissionFrame) obj;
		return Objects.equals(recipient, other.recipient);
	}

	@Override
	public String toString() {
		return "PrivateDenialTransmissionFrame{opcode=" + opcode() + ", recipient=" + recipient + "}";
	}
}
