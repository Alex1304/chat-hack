package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PublicMessageSendingFrame extends AbstractFrame {

	private final String message;

	public PublicMessageSendingFrame(String message) {
		super(Opcodes.PBS);
		this.message = requireNonNull(message);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(message)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(message);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PublicMessageSendingFrame))
			return false;
		PublicMessageSendingFrame other = (PublicMessageSendingFrame) obj;
		return Objects.equals(message, other.message);
	}

	@Override
	public String toString() {
		return "PublicMessageSendingFrame{opcode=" + opcode() + ", message=" + message + "}";
	}

	public String getMessage() {
		return message;
	}
}
