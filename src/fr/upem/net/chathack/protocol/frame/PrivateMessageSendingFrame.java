package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PrivateMessageSendingFrame extends AbstractFrame {

	private final ByteBuffer key;
	private final String message;

	public PrivateMessageSendingFrame(ByteBuffer key, String message) {
		super(Opcodes.PVM);
		this.key = requireNonNull(key);
		this.message = requireNonNull(message);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.append(key)
				.appendStringUTF8(message)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	public ByteBuffer getKey() {
		return key;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(key, message);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PrivateMessageSendingFrame))
			return false;
		PrivateMessageSendingFrame other = (PrivateMessageSendingFrame) obj;
		return Objects.equals(key, other.key) && Objects.equals(message, other.message);
	}

	@Override
	public String toString() {
		return "PrivateMessageSendingFrame{opcode=" + opcode() + ", key=" + byteBufferToString(key) + ", message="
				+ message + "}";
	}
}
