package fr.upem.net.chathack.protocol.frame;

/**
 * Visitor that can visit frames addressed to the server.
 */
public interface ServerFrameVisitor extends FrameVisitor {

	void visit(ConnectionAuthenticatedFrame frame);
	void visit(ConnectionUnauthenticatedFrame frame);
	void visit(PrivateApprovalFrame frame);
	void visit(PrivateDenialFrame frame);
	void visit(PrivateRequestFrame frame);
	void visit(PublicMessageSendingFrame frame);
	
	default void visit(PrivateFileSendingFrame frame) {}
	default void visit(PrivateMessageSendingFrame frame) {}
	
	default void visit(ConnectionApprovalFrame frame) {}
	default void visit(ConnectionDenialFrame frame) {}
	default void visit(PrivateApprovalTransmissionFrame frame) {}
	default void visit(PrivateDenialTransmissionFrame frame) {}
	default void visit(PrivateRequestErrorFrame frame) {}
	default void visit(PrivateKeyFrame frame) {}
	default void visit(PrivateRequestTransmissionFrame frame) {}
	default void visit(PublicMessageBroadcastingFrame frame) {}
}
