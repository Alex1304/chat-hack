package fr.upem.net.chathack.protocol.frame;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class ConnectionDenialFrame extends AbstractFrame {

	public static final byte USERNAME_TAKEN = 1;
	public static final byte PASSWORD_INVALID = 2;
	
	private final byte errorCode;

	public ConnectionDenialFrame(byte errorCode) {
		super(Opcodes.CRD);
		this.errorCode = errorCode;
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendByte(errorCode)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}
	
	public byte getErrorCode() {
		return errorCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(errorCode);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof ConnectionDenialFrame))
			return false;
		ConnectionDenialFrame other = (ConnectionDenialFrame) obj;
		return errorCode == other.errorCode;
	}

	@Override
	public String toString() {
		return "ConnectionDenialFrame{opcode=" + opcode() + ", errorCode=" + errorCode + "}";
	}
}
