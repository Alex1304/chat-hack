package fr.upem.net.chathack.protocol.frame;

import java.nio.ByteBuffer;

import fr.upem.net.chathack.protocol.Opcodes;

public class ConnectionApprovalFrame extends AbstractFrame {

	public ConnectionApprovalFrame() {
		super(Opcodes.CRA);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode().build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String toString() {
		return "ConnectionApprovalFrame{opcode=" + opcode() + "}";
	}
}
