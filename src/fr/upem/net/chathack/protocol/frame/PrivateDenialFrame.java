package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PrivateDenialFrame extends AbstractFrame {

	private final String sender;

	public PrivateDenialFrame(String sender) {
		super(Opcodes.PVD);
		this.sender = requireNonNull(sender);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(sender)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(sender);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PrivateDenialFrame))
			return false;
		PrivateDenialFrame other = (PrivateDenialFrame) obj;
		return Objects.equals(sender, other.sender);
	}

	@Override
	public String toString() {
		return "PrivateDenialFrame{opcode=" + opcode() + ", sender=" + sender + "}";
	}

	public String getSender() {
		return sender;
	}
}
