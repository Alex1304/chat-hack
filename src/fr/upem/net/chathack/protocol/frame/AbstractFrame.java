package fr.upem.net.chathack.protocol.frame;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.ByteBufferBuilder;
import fr.upem.net.chathack.protocol.Opcodes;

abstract class AbstractFrame implements Frame {

	private final byte opcode;
	
	AbstractFrame(byte opcode) {
		if (!Opcodes.isValid(opcode)) {
			throw new IllegalArgumentException("Invalid opcode " + opcode);
		}
		this.opcode = opcode;
	}
	
	/**
	 * Gets the opcode of this frame. It can be one of the constants declared in
	 * {@link Opcodes}.
	 * 
	 * @return the opcode
	 */
	public byte opcode() {
		return opcode;
	}
	
	ByteBufferBuilder newByteBufferBuilderWithOpcode() {
		return new ByteBufferBuilder().appendByte(opcode);
	}
	
	String byteBufferToString(ByteBuffer buf) {
		final var max = 32;
		var sb = new StringBuilder();
		var buf2 = buf.duplicate();
		var read = 0;
		while (read < max && buf2.hasRemaining()) {
			sb.append(String.format("%02x", buf2.get() & 0xff));
			sb.append(' ');
			read++;
		}
		if (buf2.hasRemaining()) {
			sb.deleteCharAt(sb.length() - 1);
			sb.append("...");
		}
		sb.append(" (total ").append(buf.remaining()).append(" bytes)");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(opcode);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof AbstractFrame))
			return false;
		AbstractFrame other = (AbstractFrame) obj;
		return opcode == other.opcode;
	}
}
