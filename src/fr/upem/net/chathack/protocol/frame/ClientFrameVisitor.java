package fr.upem.net.chathack.protocol.frame;

/**
 * Visitor that can visit frames addressed to the client.
 */
public interface ClientFrameVisitor extends FrameVisitor {

	void visit(ConnectionApprovalFrame frame);
	void visit(ConnectionDenialFrame frame);
	void visit(PrivateApprovalTransmissionFrame frame);
	void visit(PrivateDenialTransmissionFrame frame);
	void visit(PrivateRequestErrorFrame frame);
	void visit(PrivateKeyFrame frame);
	void visit(PrivateRequestTransmissionFrame frame);
	void visit(PublicMessageBroadcastingFrame frame);
	
	default void visit(PrivateFileSendingFrame frame) {}
	default void visit(PrivateMessageSendingFrame frame) {}
	
	default void visit(ConnectionAuthenticatedFrame frame) {}
	default void visit(ConnectionUnauthenticatedFrame frame) {}
	default void visit(PrivateApprovalFrame frame) {}
	default void visit(PrivateDenialFrame frame) {}
	default void visit(PrivateRequestFrame frame) {}
	default void visit(PublicMessageSendingFrame frame) {}
}
