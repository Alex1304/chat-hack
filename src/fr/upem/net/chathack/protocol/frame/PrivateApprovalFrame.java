package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PrivateApprovalFrame extends AbstractFrame {

	private final String sender;
	private final int port;

	public PrivateApprovalFrame(String sender, int port) {
		super(Opcodes.PVA);
		this.sender = requireNonNull(sender);
		this.port = port;
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(sender)
				.appendInt(port)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(port, sender);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PrivateApprovalFrame))
			return false;
		PrivateApprovalFrame other = (PrivateApprovalFrame) obj;
		return port == other.port && Objects.equals(sender, other.sender);
	}

	@Override
	public String toString() {
		return "PrivateApprovalFrame{opcode=" + opcode() + ", sender=" + sender + ", port=" + port + "}";
	}

	public String getSender() {
		return sender;
	}

	public int getPort() {
		return port;
	}
	
}
