package fr.upem.net.chathack.protocol.frame;

/**
 * Visitor that can visit frames exchanged during a private connection between two clients.
 */
public interface PrivateClientFrameVisitor extends FrameVisitor {

	void visit(PrivateFileSendingFrame frame);
	void visit(PrivateMessageSendingFrame frame);
	
	default void visit(ConnectionApprovalFrame frame) {}
	default void visit(ConnectionDenialFrame frame) {}
	default void visit(PrivateApprovalTransmissionFrame frame) {}
	default void visit(PrivateDenialTransmissionFrame frame) {}
	default void visit(PrivateRequestErrorFrame frame) {}
	default void visit(PrivateKeyFrame frame) {}
	default void visit(PrivateRequestTransmissionFrame frame) {}
	default void visit(PublicMessageBroadcastingFrame frame) {}
	
	default void visit(ConnectionAuthenticatedFrame frame) {}
	default void visit(ConnectionUnauthenticatedFrame frame) {}
	default void visit(PrivateApprovalFrame frame) {}
	default void visit(PrivateDenialFrame frame) {}
	default void visit(PrivateRequestFrame frame) {}
	default void visit(PublicMessageSendingFrame frame) {}
}
