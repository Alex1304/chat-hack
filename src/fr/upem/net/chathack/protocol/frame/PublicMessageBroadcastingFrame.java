package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PublicMessageBroadcastingFrame extends AbstractFrame {

	private final String author;
	private final String message;

	public PublicMessageBroadcastingFrame(String author, String message) {
		super(Opcodes.PBB);
		this.author = requireNonNull(author);
		this.message = requireNonNull(message);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(author)
				.appendStringUTF8(message)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	public String getAuthor() {
		return author;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(author, message);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PublicMessageBroadcastingFrame))
			return false;
		PublicMessageBroadcastingFrame other = (PublicMessageBroadcastingFrame) obj;
		return Objects.equals(author, other.author) && Objects.equals(message, other.message);
	}

	@Override
	public String toString() {
		return "PublicMessageBroadcastingFrame{opcode=" + opcode() + ", author=" + author + ", message=" + message
				+ "}";
	}
}
