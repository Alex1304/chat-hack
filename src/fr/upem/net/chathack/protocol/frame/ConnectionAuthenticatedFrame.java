package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class ConnectionAuthenticatedFrame extends AbstractFrame {

	private final String username;
	private final String password;

	public ConnectionAuthenticatedFrame(String username, String password) {
		super(Opcodes.CNP);
		this.username = requireNonNull(username);
		this.password = requireNonNull(password);
	}
	
	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(username)
				.appendStringUTF8(password)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(password, username);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof ConnectionAuthenticatedFrame))
			return false;
		ConnectionAuthenticatedFrame other = (ConnectionAuthenticatedFrame) obj;
		return Objects.equals(password, other.password) && Objects.equals(username, other.username);
	}

	@Override
	public String toString() {
		return "AuthenticatedConnectionFrame{opcode=" + opcode() + ", username=" + username + ", password=" + password
				+ "}";
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}
