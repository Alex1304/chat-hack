package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PrivateRequestTransmissionFrame extends AbstractFrame {

	private final String sender;

	public PrivateRequestTransmissionFrame(String sender) {
		super(Opcodes.PVRT);
		this.sender = requireNonNull(sender);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(sender)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	public String getSender() {
		return sender;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(sender);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PrivateRequestTransmissionFrame))
			return false;
		PrivateRequestTransmissionFrame other = (PrivateRequestTransmissionFrame) obj;
		return Objects.equals(sender, other.sender);
	}

	@Override
	public String toString() {
		return "PrivateRequestTransmissionFrame{opcode=" + opcode() + ", sender=" + sender + "}";
	}
}
