package fr.upem.net.chathack.protocol.frame;

/**
 * Visitor for all existing Frame implementations.
 */
public interface FrameVisitor {
	
	void visit(ConnectionApprovalFrame frame);
	void visit(ConnectionAuthenticatedFrame frame);
	void visit(ConnectionDenialFrame frame);
	void visit(ConnectionUnauthenticatedFrame frame);
	void visit(PrivateApprovalFrame frame);
	void visit(PrivateDenialFrame frame);
	void visit(PrivateFileSendingFrame frame);
	void visit(PrivateMessageSendingFrame frame);
	void visit(PrivateApprovalTransmissionFrame frame);
	void visit(PrivateDenialTransmissionFrame frame);
	void visit(PrivateRequestErrorFrame frame);
	void visit(PrivateRequestFrame frame);
	void visit(PrivateKeyFrame frame);
	void visit(PrivateRequestTransmissionFrame frame);
	void visit(PublicMessageBroadcastingFrame frame);
	void visit(PublicMessageSendingFrame frame);
}
