package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class ConnectionUnauthenticatedFrame extends AbstractFrame {

	private final String username;

	public ConnectionUnauthenticatedFrame(String username) {
		super(Opcodes.CNU);
		this.username = requireNonNull(username);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(username)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(username);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof ConnectionUnauthenticatedFrame))
			return false;
		ConnectionUnauthenticatedFrame other = (ConnectionUnauthenticatedFrame) obj;
		return Objects.equals(username, other.username);
	}

	@Override
	public String toString() {
		return "UnauthenticatedConnectionFrame{opcode=" + opcode() + ", username=" + username + "}";
	}

	public String getUsername() {
		return username;
	}
}
