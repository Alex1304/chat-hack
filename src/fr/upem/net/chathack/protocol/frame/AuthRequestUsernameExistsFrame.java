package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.ByteBufferBuilder;

public class AuthRequestUsernameExistsFrame implements Frame {
	
	private final long id;
	private final String username;
	
	public AuthRequestUsernameExistsFrame(long id, String username) {
		this.id = requireNonNull(id);
		this.username = requireNonNull(username);
	}
	
	@Override
	public ByteBuffer encode() {
		return new ByteBufferBuilder()
				.appendByte((byte)2)
				.appendLong(id)
				.appendStringUTF8(username)
				.build();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(id, username);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof AuthRequestLoginFrame))
			return false;
		AuthRequestUsernameExistsFrame other = (AuthRequestUsernameExistsFrame) obj;
		return Objects.equals(id, other.id)
				&& Objects.equals(username, other.username);
	}
	
	@Override
	public String toString() {
		return "AuthRequestUsernameExistsFrame{id=" + id + ","
				+ ", username=" + username
				+ "}";
	}

	public long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public void accept(FrameVisitor visitor) {
		return;
	}
}
