package fr.upem.net.chathack.protocol.frame;

import java.nio.ByteBuffer;

/**
 * Interface that represents a frame as defined in the chat hack protocol.
 */
public interface Frame {
	
	/**
	 * Encodes this frame into a byte buffer. A new instance is returned on each
	 * call. The returned byte buffer is in read-mode.
	 * 
	 * @return a byte buffer containing the encoded data of this frame
	 */
	ByteBuffer encode();
	
	/**
	 * Accepts a frame visitor, calling the visit method corresponding to the real
	 * type of this frame.
	 * 
	 * @param visitor the visitor to accept
	 */
	void accept(FrameVisitor visitor);
}
