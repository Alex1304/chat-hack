package fr.upem.net.chathack.protocol.frame;

import java.nio.ByteBuffer;
import java.util.Objects;

public class AuthResponseFrame implements Frame {

	private byte response;
	private long id;
	
	public AuthResponseFrame(byte response, long id) {
		this.response = response;
		this.id = id;
	}

	public void accept(AuthFrameVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(id);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof AuthResponseFrame))
			return false;
		AuthResponseFrame other = (AuthResponseFrame) obj;
		return Objects.equals(id, other.id);
	}
	
	@Override
	public String toString() {
		return "AuthResponseFrame{id=" + id + "}";
	}

	public long getResponse() {
		return response;
	}
	
	public long getId() {
		return id;
	}

	@Override
	public ByteBuffer encode() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		if (visitor instanceof AuthFrameVisitor) {
			((AuthFrameVisitor) visitor).visit(this);
		}
	}
}
