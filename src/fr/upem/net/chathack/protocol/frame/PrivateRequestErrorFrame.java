package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PrivateRequestErrorFrame extends AbstractFrame {

	private final String recipient;

	public PrivateRequestErrorFrame(String recipient) {
		super(Opcodes.PVRE);
		this.recipient = requireNonNull(recipient);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(recipient)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(recipient);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PrivateRequestErrorFrame))
			return false;
		PrivateRequestErrorFrame other = (PrivateRequestErrorFrame) obj;
		return Objects.equals(recipient, other.recipient);
	}

	@Override
	public String toString() {
		return "PrivateRequestErrorFrame{opcode=" + opcode() + ", recipient=" + recipient + "}";
	}

	public String getRecipient() {
		return recipient;
	}
}
