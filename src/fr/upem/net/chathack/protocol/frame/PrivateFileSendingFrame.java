package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PrivateFileSendingFrame extends AbstractFrame {

	private final ByteBuffer key;
	private final String filename;
	private final ByteBuffer data;

	public PrivateFileSendingFrame(ByteBuffer key, String filename, ByteBuffer data) {
		super(Opcodes.PVF);
		this.key = requireNonNull(key);
		this.filename = requireNonNull(filename);
		this.data = requireNonNull(data);
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.append(key)
				.appendStringUTF8(filename)
				.appendWithSize(data)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	public ByteBuffer getKey() {
		return key;
	}

	public String getFilename() {
		return filename;
	}

	public ByteBuffer getData() {
		return data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(data, filename, key);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PrivateFileSendingFrame))
			return false;
		PrivateFileSendingFrame other = (PrivateFileSendingFrame) obj;
		return Objects.equals(data, other.data) && Objects.equals(filename, other.filename)
				&& Objects.equals(key, other.key);
	}

	@Override
	public String toString() {
		return "PrivateFileSendingFrame{opcode=" + opcode() + ", key=" + byteBufferToString(key) + ", filename=" + filename + ", data="
				+ byteBufferToString(data) + "}";
	}
}
