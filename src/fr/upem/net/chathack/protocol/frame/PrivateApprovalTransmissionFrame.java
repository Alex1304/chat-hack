package fr.upem.net.chathack.protocol.frame;

import static java.util.Objects.requireNonNull;

import java.net.Inet4Address;
import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.net.chathack.protocol.Opcodes;

public class PrivateApprovalTransmissionFrame extends AbstractFrame {

	private final String recipient;
	private final ByteBuffer key;
	private final Inet4Address ip;
	private final int port;

	public PrivateApprovalTransmissionFrame(String recipient, ByteBuffer key, Inet4Address ip, int port) {
		super(Opcodes.PVAT);
		this.recipient = requireNonNull(recipient);
		this.key = requireNonNull(key);
		this.ip = requireNonNull(ip);
		this.port = port;
	}

	@Override
	public ByteBuffer encode() {
		return newByteBufferBuilderWithOpcode()
				.appendStringUTF8(recipient)
				.append(key)
				.append(ByteBuffer.wrap(ip.getAddress()))
				.appendInt(port)
				.build();
	}

	@Override
	public void accept(FrameVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(ip, key, port, recipient);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PrivateApprovalTransmissionFrame))
			return false;
		PrivateApprovalTransmissionFrame other = (PrivateApprovalTransmissionFrame) obj;
		return Objects.equals(ip, other.ip) && Objects.equals(key, other.key) && port == other.port
				&& Objects.equals(recipient, other.recipient);
	}

	@Override
	public String toString() {
		return "PrivateApprovalTransmissionFrame{opcode=" + opcode() + ", recipient=" + recipient + ", key="
				+ byteBufferToString(key) + ", ip=" + ip + ", port=" + port + "}";
	}

	public String getRecipient() {
		return recipient;
	}

	public ByteBuffer getKey() {
		return key;
	}

	public Inet4Address getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}
	
}
