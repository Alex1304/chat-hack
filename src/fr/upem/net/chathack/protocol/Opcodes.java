package fr.upem.net.chathack.protocol;

/**
 * Opcodes defined in Chat Hack Protocol.
 */
public class Opcodes {
	
	/**
	 * Connection Request, Username Only
	 */
	public static final byte CNU = 1;

	/**
	 * Connection Request, Username and Password
	 */
	public static final byte CNP = 2;

	/**
	 * Public Message Sending
	 */
	public static final byte PBS = 3;

	/**
	 * Private Connection Request
	 */
	public static final byte PVR = 4;

	/**
	 * Private Connection Approval
	 */
	public static final byte PVA = 5;

	/**
	 * Private Connection Denial
	 */
	public static final byte PVD = 6;

	/**
	 * Private Message Sending
	 */
	public static final byte PVM = 7;

	/**
	 * Private File Sending
	 */
	public static final byte PVF = 8;

	/**
	 * Connection Request Approval
	 */
	public static final byte CRA = 9;

	/**
	 * Connection Request Denial
	 */
	public static final byte CRD = 10;

	/**
	 * Public Message Broadcasting
	 */
	public static final byte PBB = 11;

	/**
	 * Private Connection Request Error
	 */
	public static final byte PVRE = 12;

	/**
	 * Private Connection Request Transmission
	 */
	public static final byte PVRT = 13;

	/**
	 * Private Connection Approval Transmission
	 */
	public static final byte PVAT = 14;

	/**
	 * Private Connection Denial Transmission
	 */
	public static final byte PVDT = 15;

	/**
	 * Private Connection Key
	 */
	public static final byte PVK = 16;
	
	public static boolean isValid(byte value) {
		return value >= CNU && value <= PVK;
	}
	
	private Opcodes() {
		throw new AssertionError();
	}
}
