package fr.upem.net.chathack.protocol;

import static java.nio.channels.SelectionKey.OP_READ;
import static java.nio.channels.SelectionKey.OP_WRITE;
import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.StringJoiner;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.upem.net.chathack.protocol.frame.Frame;
import fr.upem.net.chathack.protocol.frame.FrameVisitor;
import fr.upem.net.chathack.protocol.reader.DecodeException;
import fr.upem.net.chathack.protocol.reader.Reader;

/**
 * Bridge between Frame API and non-blocking I/O API.
 */
public class NetworkContext {
	private static final int BUFFER_SIZE = 1_024;
	private static final Logger LOGGER = Logger.getLogger(NetworkContext.class.getName());
	
	private final SelectionKey key;
	private final SocketChannel socket;
	private final Reader<? extends Frame> frameReader;
	private final ByteBuffer bbin = ByteBuffer.allocateDirect(BUFFER_SIZE);
	private final ByteBuffer bbout = ByteBuffer.allocateDirect(BUFFER_SIZE);
	private final Queue<EncodedFrame> queue = new ArrayDeque<>();
	private final List<PendingCallback> pendingCallbacks = new ArrayList<>();
	private final boolean debug;
	private boolean closed = false;
	private final FrameVisitor visitor;
	private Runnable onClose;
	
	public NetworkContext(SelectionKey key, Reader<? extends Frame> frameReader, FrameVisitor visitor, boolean debug) {
		this.key = requireNonNull(key);
		this.frameReader = requireNonNull(frameReader);
		this.visitor = requireNonNull(visitor);
		this.socket = (SocketChannel) key.channel();
		this.debug = debug;
	}
	
	private void processIn() {
		try {
			while (bbin.position() > 0) {
				bbin.flip();
				var readResult = frameReader.read(bbin);
				bbin.compact();
				readResult.ifPresent(frame -> {
					if (debug) {
						LOGGER.info("Decoded frame " + frame);
					}
					frame.accept(visitor);
				});
			}
		} catch (DecodeException e) {
			LOGGER.log(Level.WARNING, "Unable to decode frame", e);
			close();
		} catch (RuntimeException e) {
			LOGGER.log(Level.WARNING, "Something went wrong when processing frame", e);
			close();
		}
	}

	/**
	 * Add a frame to the queue.
	 *
	 * @param frame the frame to add
	 */
	public void queueFrame(Frame frame) {
		queueFrame(frame, null);
	}

	/**
	 * Add a frame to the queue. When the frame is fully written to the socket, the
	 * callback will be called.
	 *
	 * @param frame           the frame to add
	 * @param onDataProcessed a callback to invoke when the frame is fully written
	 *                        in the socket
	 */
	public void queueFrame(Frame frame, Runnable onDataProcessed) {
		if (debug) {
			LOGGER.info("Queued frame " + frame);
		}
		queue.add(new EncodedFrame(frame.encode(), onDataProcessed));
		processOut();
		updateInterestOps();
	}
	
	/**
	 * Sets a callback to invoke when the connection closes.
	 * 
	 * @param onClose the callback
	 */
	public void setCloseHandler(Runnable onClose) {
	    this.onClose = onClose;
	}

	private void processOut() {
		while (!queue.isEmpty() && bbout.hasRemaining()) {
			var head = queue.element();
			if (head.data.hasRemaining()) {
				bbout.put(head.data.get());
			} else {
				queue.remove();
				if (head.onDataProcessed != null) {
					pendingCallbacks.add(new PendingCallback(head.onDataProcessed, bbout.position()));
				}
			}
		}
	}
	
	private void processPendingCallbacks(int bytesWritten) {
		var it = pendingCallbacks.iterator();
		while (it.hasNext()) {
			var pendingCallback = it.next();
			pendingCallback.bytesRemaining = pendingCallback.bytesRemaining - bytesWritten;
			if (pendingCallback.bytesRemaining <= 0) {
				pendingCallback.callback.run();
				it.remove();
			}
		}
	}

	private void updateInterestOps() {
		if (!key.isValid()) {
			close();
			return;
		}
		var updatedOps = 0;

		if (!closed && bbin.hasRemaining()) {
			updatedOps |= OP_READ;
		}
		if (bbout.position() != 0) {
			updatedOps |= OP_WRITE;
		}
		if (debug) {
			var ops = new StringJoiner(", ", "[", "]");
			if ((updatedOps & OP_READ) != 0) {
				ops.add("OP_READ");
			}
			if ((updatedOps & OP_WRITE) != 0) {
				ops.add("OP_WRITE");
			}
			LOGGER.info("New interest ops: " + ops.toString());
		}
		if (updatedOps == 0) {
			close();
		} else {
			key.interestOps(updatedOps);
		}
	}

	public void close() {
		try {
			if (debug) {
				LOGGER.info("Closing connection");
			}
			socket.close();
		} catch (IOException e) {
			// ignore exception
		} finally {
			if (onClose != null) {
				onClose.run();
			}
		}
	}

	/**
	 * Performs the read action on the socket.
	 *
	 * @throws IOException if an I/O error occurs
	 */
	public void doRead() throws IOException {
		var bytesRead = socket.read(bbin);
		if (bytesRead == -1) {
			closed = true;
		}
		if (debug) {
			LOGGER.info("Read " + bytesRead + " bytes from " + getRemoteAddress());
		}
		processIn();
		updateInterestOps();
	}

	/**
	 * Performs the write action on the socket.
	 *
	 * @throws IOException if an I/O error occurs
	 */
	public void doWrite() throws IOException {
		bbout.flip();
		var bytesWritten = socket.write(bbout);
		if (debug) {
			LOGGER.info("Wrote " + bytesWritten + " bytes to " + getRemoteAddress());
		}
		bbout.compact();
		processPendingCallbacks(bytesWritten);
		processOut();
		updateInterestOps();
	}
	
	/**
	 * Gets the IPv4 address of the remote node it is connected to.
	 * 
	 * @return the IPv4 address
	 */
	public Inet4Address getRemoteAddress() {
		try {
			return (Inet4Address) ((InetSocketAddress) socket.getRemoteAddress()).getAddress();
		} catch (ClassCastException | IOException e) {
			close();
			throw new RuntimeException(e);
		}
	}
	
	private static class EncodedFrame {
		private final ByteBuffer data;
		private final Runnable onDataProcessed;
		
		private EncodedFrame(ByteBuffer data, Runnable onDataProcessed) {
			this.data = data;
			this.onDataProcessed = onDataProcessed;
		}
	}
	
	private static class PendingCallback {
		private final Runnable callback;
		private int bytesRemaining;
		
		private PendingCallback(Runnable callback, int bytesRemaining) {
			this.callback = callback;
			this.bytesRemaining = bytesRemaining;
		}
	}
}