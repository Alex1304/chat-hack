package fr.upem.net.chathack.server;

/**
 * Data class for a login request.
 */
public class LoginRequest {

	private final String username;
	private final boolean hasPassword;
	
	public LoginRequest(String username, boolean hasPassword) {
		this.username = username;
		this.hasPassword = hasPassword;
	}

	public String getUsername() {
		return username;
	}

	public boolean hasPassword() {
		return hasPassword;
	}
}
