package fr.upem.net.chathack.server;

import static fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame.PASSWORD_INVALID;
import static fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame.USERNAME_TAKEN;

import java.util.logging.Logger;

import fr.upem.net.chathack.protocol.NetworkContext;
import fr.upem.net.chathack.protocol.frame.AuthFrameVisitor;
import fr.upem.net.chathack.protocol.frame.AuthResponseFrame;
import fr.upem.net.chathack.protocol.frame.ConnectionApprovalFrame;
import fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame;

/**
 * Handle that represents the connection to the auth server.
 */
public class AuthServerHandle implements AuthFrameVisitor {

	private static final Logger LOGGER = Logger.getLogger(AuthServerHandle.class.getName());
	private final ServerState state;
	private NetworkContext networkContext;
	
	public AuthServerHandle(ServerState state) {
		this.state = state;
	}
	
	public NetworkContext getNetworkContext() {
		return networkContext;
	}
	
	public void setNetworkContext(NetworkContext networkContext) {
		this.networkContext = networkContext;
	}

	@Override
	public void visit(AuthResponseFrame frame) {
		var loginRequest = state.pendingLoginRequests().remove(frame.getId());
		if (loginRequest == null) {
			LOGGER.warning("Discarding unmatched auth response frame " + frame);
			return;
		}
		state.withConnectedUserHandle(loginRequest.getUsername(), handle -> {
			var okResponse = loginRequest.hasPassword() ? 1 : 0;
			if (frame.getResponse() == okResponse) {
				handle.getNetworkContext().queueFrame(new ConnectionApprovalFrame());
				LOGGER.info("Successfully authenticated user " + loginRequest.getUsername()
						+ " (with password = " + loginRequest.hasPassword() + ")");
			} else {
				var errorCode = loginRequest.hasPassword() ? PASSWORD_INVALID : USERNAME_TAKEN;
				handle.getNetworkContext().queueFrame(new ConnectionDenialFrame(errorCode), handle.getNetworkContext()::close);
			}
		});
	}
}
