package fr.upem.net.chathack.server;

import java.util.HashMap;
import java.util.function.Consumer;

import fr.upem.net.chathack.protocol.frame.PublicMessageBroadcastingFrame;

/**
 * Manages the state of the server.
 */
public class ServerState {
	
	private final HashMap<String, ClientHandle> connectedUsers = new HashMap<>();
	private final HashMap<Long, LoginRequest> pendingLoginRequests = new HashMap<>();
	
	private AuthServerHandle authServerHandle;
	
	public void setAuthServerHandle(AuthServerHandle authServerHandle) {
		this.authServerHandle = authServerHandle;
	}

	public void withAuthServerHandle(Consumer<AuthServerHandle> action) {
		if (authServerHandle != null) {
			action.accept(authServerHandle);
		} else {
			throw new IllegalStateException("Not yet connected to auth server");
		}
	}
	
	public void withConnectedUserHandle(String username, Consumer<ClientHandle> action) {
		var handle = connectedUsers.get(username);
		if (handle != null) {
			action.accept(handle);
		} else {
			throw new IllegalStateException("No user with name " + username + " is connected");
		}
	}
	
	public void broadcastPublicMessage(PublicMessageBroadcastingFrame frame) {
		for (var clientHandle : connectedUsers.values()) {
			// Broadcast to everyone except the author
			if (frame.getAuthor().equals(clientHandle.getUsername())) {
				continue;
			}
			clientHandle.getNetworkContext().queueFrame(frame);
		}
	}

	public HashMap<String, ClientHandle> connectedUsers() {
		return connectedUsers;
	}

	public HashMap<Long, LoginRequest> pendingLoginRequests() {
		return pendingLoginRequests;
	}
}
