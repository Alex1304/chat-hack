package fr.upem.net.chathack.server;

import static java.nio.channels.SelectionKey.OP_READ;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.upem.net.chathack.protocol.NetworkContext;
import fr.upem.net.chathack.protocol.reader.AuthResponseFrameReader;
import fr.upem.net.chathack.protocol.reader.ChatHackFrameReader;

/**
 * Main class for server.
 */
public class Server {
	
	private static final Logger LOGGER = Logger.getLogger(Server.class.getName());

	private final ServerSocketChannel serverSocket;
	private final Selector selector;
	private final ServerState state;
	
	private Server(ServerSocketChannel serverSocket, Selector selector, ServerState state) {
		this.serverSocket = serverSocket;
		this.selector = selector;
		this.state = state;
	}
	
	public static Server create(int serverPort, SocketAddress authServerAddress) throws IOException {
		// Configure server socket
		var serverSocket = ServerSocketChannel.open();
		serverSocket.bind(new InetSocketAddress(serverPort));
		serverSocket.configureBlocking(false);
		// Configure socket to auth server
		var authSocket = SocketChannel.open();
		authSocket.configureBlocking(false);
		authSocket.connect(authServerAddress);
		// Configure selector
		var selector = Selector.open();
		serverSocket.register(selector, SelectionKey.OP_ACCEPT);
		authSocket.register(selector, SelectionKey.OP_CONNECT);
		// Configure state
		var state = new ServerState();
		return new Server(serverSocket, selector, state);
	}

	public static void usage() {
		System.out.println("Usage : Server server_port auth_server_address auth_server_port");
		return;
	}
	
	public static void main(String[] args) throws IOException {
		System.out.println("Chat Hack Server");
		// parse args
		if (args.length != 3) {
			usage();
			return;
		}
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		int serverPort = Integer.parseInt(args[0]);
		int authServerPort = Integer.parseInt(args[2]);
		SocketAddress auhServerAddress = new InetSocketAddress(args[1], authServerPort);
		// init server
		Server server = create(serverPort, auhServerAddress);
		LOGGER.info("Server started on port " + serverPort);
		server.launch();
	}

	public void launch() throws IOException {
		while (!Thread.interrupted()) {
			try {
				selector.select(this::treatKey);
			} catch (UncheckedIOException e) {
				throw e.getCause();
			}
		}
	}

	private void treatKey(SelectionKey key) {
		try {
			if (key.isValid() && key.isAcceptable()) {
				doAccept(key);
			}
			if (key.isValid() && key.isConnectable()) {
				doConnect(key);
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		try {
			if (key.isValid() && key.isWritable()) {
				((NetworkContext) key.attachment()).doWrite();
			}
			if (key.isValid() && key.isReadable()) {
				((NetworkContext) key.attachment()).doRead();
			}
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Closing due to IOException", e);
			silentlyClose(key);
		}
	}

	private void doConnect(SelectionKey key) throws IOException {
		var authSocket = (SocketChannel) key.channel();
		if (!authSocket.finishConnect()) {
			return;
		}
		key.interestOps(OP_READ);
		var handle = new AuthServerHandle(state);
		var networkContext = new NetworkContext(key, new AuthResponseFrameReader(), handle, true);
		handle.setNetworkContext(networkContext);
		state.setAuthServerHandle(handle);
		key.attach(networkContext);
		LOGGER.info("Successfully connected to auth server");
	}

	private void doAccept(SelectionKey key) throws IOException {
		SocketChannel sc = serverSocket.accept();
		if (sc == null) {
			return;
		}
		sc.configureBlocking(false);
		SelectionKey clientKey = sc.register(selector, OP_READ);
		var handle = new ClientHandle(state);
		var networkContext = new NetworkContext(clientKey, new ChatHackFrameReader(), handle, true);
		handle.setNetworkContext(networkContext);
		clientKey.attach(networkContext);
		LOGGER.info("Accepted client " + networkContext.getRemoteAddress());
	}

	private static void silentlyClose(SelectionKey key) {
		Channel sc = key.channel();
		try {
			sc.close();
		} catch (IOException e) {
			// ignore exception
		}
	}
}
