package fr.upem.net.chathack.server;

import static fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame.USERNAME_TAKEN;

import java.nio.ByteBuffer;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import fr.upem.net.chathack.protocol.NetworkContext;
import fr.upem.net.chathack.protocol.frame.AuthRequestLoginFrame;
import fr.upem.net.chathack.protocol.frame.AuthRequestUsernameExistsFrame;
import fr.upem.net.chathack.protocol.frame.ConnectionAuthenticatedFrame;
import fr.upem.net.chathack.protocol.frame.ConnectionDenialFrame;
import fr.upem.net.chathack.protocol.frame.ConnectionUnauthenticatedFrame;
import fr.upem.net.chathack.protocol.frame.PrivateApprovalFrame;
import fr.upem.net.chathack.protocol.frame.PrivateApprovalTransmissionFrame;
import fr.upem.net.chathack.protocol.frame.PrivateDenialFrame;
import fr.upem.net.chathack.protocol.frame.PrivateDenialTransmissionFrame;
import fr.upem.net.chathack.protocol.frame.PrivateKeyFrame;
import fr.upem.net.chathack.protocol.frame.PrivateRequestErrorFrame;
import fr.upem.net.chathack.protocol.frame.PrivateRequestFrame;
import fr.upem.net.chathack.protocol.frame.PrivateRequestTransmissionFrame;
import fr.upem.net.chathack.protocol.frame.PublicMessageBroadcastingFrame;
import fr.upem.net.chathack.protocol.frame.PublicMessageSendingFrame;
import fr.upem.net.chathack.protocol.frame.ServerFrameVisitor;

/**
 * Handle that represents the connection to a client.
 */
public class ClientHandle implements ServerFrameVisitor {

	private static final AtomicLong ID_GENERATOR = new AtomicLong();
	
	private final ServerState state;
	private NetworkContext networkContext;
	private String username;

	public ClientHandle(ServerState state) {
		this.state = state;
	}
	
	public NetworkContext getNetworkContext() {
		return networkContext;
	}
	
	public void setNetworkContext(NetworkContext context) {
		this.networkContext = context;
	}
	
	public String getUsername() {
		return username;
	}
	
	@Override
	public void visit(ConnectionUnauthenticatedFrame frame) {
		checkLoggedIn(false);
		username = frame.getUsername();
		if (state.connectedUsers().putIfAbsent(username, this) != null) {
			networkContext.queueFrame(new ConnectionDenialFrame(USERNAME_TAKEN), networkContext::close);
		} else {
			networkContext.setCloseHandler(() -> state.connectedUsers().remove(username));
			var id = ID_GENERATOR.incrementAndGet();
			state.pendingLoginRequests().put(id, new LoginRequest(username, false));
			state.withAuthServerHandle(handle -> handle.getNetworkContext()
					.queueFrame(new AuthRequestUsernameExistsFrame(id, username)));
		}
	}

	@Override
	public void visit(ConnectionAuthenticatedFrame frame) {
		checkLoggedIn(false);
		username = frame.getUsername();
		if (state.connectedUsers().putIfAbsent(username, this) != null) {
			networkContext.queueFrame(new ConnectionDenialFrame(USERNAME_TAKEN), networkContext::close);
		}
		networkContext.setCloseHandler(() -> state.connectedUsers().remove(username));
		var id = ID_GENERATOR.incrementAndGet();
		state.pendingLoginRequests().put(id, new LoginRequest(frame.getUsername(), true));
		state.withAuthServerHandle(handle -> handle.getNetworkContext()
				.queueFrame(new AuthRequestLoginFrame(id, frame.getUsername(), frame.getPassword())));
	}

	@Override
	public void visit(PublicMessageSendingFrame frame) {
		checkLoggedIn(true);
		state.broadcastPublicMessage(new PublicMessageBroadcastingFrame(username, frame.getMessage()));
	}

	@Override
	public void visit(PrivateRequestFrame frame) {
		checkLoggedIn(true);
		if (state.connectedUsers().containsKey(frame.getRecipient())) {
			state.withConnectedUserHandle(frame.getRecipient(), handle -> handle.networkContext
					.queueFrame(new PrivateRequestTransmissionFrame(username)));
		} else {
			networkContext.queueFrame(new PrivateRequestErrorFrame(frame.getRecipient()));
		}
	}

	@Override
	public void visit(PrivateDenialFrame frame) {
		checkLoggedIn(true);
		state.withConnectedUserHandle(frame.getSender(), handle -> handle.networkContext
				.queueFrame(new PrivateDenialTransmissionFrame(username)));
	}

	@Override
	public void visit(PrivateApprovalFrame frame) {
		checkLoggedIn(true);
		byte[] b = new byte[16];
		new Random().nextBytes(b);
		ByteBuffer key = ByteBuffer.wrap(b);
		networkContext.queueFrame(new PrivateKeyFrame(frame.getSender(), key));
		state.withConnectedUserHandle(frame.getSender(), handle -> handle.networkContext.queueFrame(
				new PrivateApprovalTransmissionFrame(username, key, networkContext.getRemoteAddress(), frame.getPort())));
	}
	
	void checkLoggedIn(boolean expectation) {
	    if (expectation ? username == null : username != null) {
	        networkContext.close();
	    }
	}
}
